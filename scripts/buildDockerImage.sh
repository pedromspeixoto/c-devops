# Script to build a docker image from a specified path

# Set build variables
USER=$1
IMAGE_NAME=$2
IMAGE_PATH=$3
VERSION="$(git log -1 --pretty=%h)"

# Run the docker build command
docker build -t $USER/$IMAGE_NAME:$VERSION $IMAGE_PATH

# Remove previous version defined as latest
docker image rm $USER/$IMAGE_NAME:latest

# Set this as the latest version
docker image tag $USER/$IMAGE_NAME:$VERSION $USER/$IMAGE_NAME:latest

# Push images to the Docker repository
docker push $USER/$IMAGE_NAME
